const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid')
const config = require('../config');
const fileDb = require('../fileDb');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
  const files = fileDb.getItems();
  res.send(files);
});

router.get('/:id', (req, res) => {
  const files = fileDb.getItem(req.params.id);
  if (!files) {
    return res.status(404).send({error: 'File not found'});
  }
  res.send(files);
});

router.post('/', upload.single('image'),(req, res) => {
  if (req.body.message === '') {
    res.send({error: 'Message field should not be empty!'});
  } else {
    const card = req.body;

    if(req.file) {
      card.image = req.file.filename;
    } else {
      card.image = null;
    }
    const newCard = fileDb.addItem(card)
    res.send(newCard);
  }
});

module.exports = router; // export default router;