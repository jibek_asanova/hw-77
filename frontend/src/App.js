import ImageBoard from "./container/ImageBoard/ImageBoard";

const App = () => (
    <div className="App">
        <ImageBoard/>
    </div>
);

export default App;
