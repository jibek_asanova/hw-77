import {FETCH_CARDS_FAILURE, FETCH_CARDS_REQUEST, FETCH_CARDS_SUCCESS} from "./actions";

const initialState = {
    fetchLoading: false,
    cards: [],
}

const cardsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CARDS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_CARDS_SUCCESS:
            return {...state, fetchLoading: false, cards: action.payload};
        case FETCH_CARDS_FAILURE:
            return {...state, fetchLoading: false};
        default:
            return state;
    }
};

export default cardsReducer;