import axios from "axios";

export const FETCH_CARDS_REQUEST = 'FETCH_CARDS_REQUEST';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_FAILURE = 'FETCH_CARDS_FAILURE';

export const CREATE_CARD_REQUEST = 'CREATE_CARD_REQUEST';
export const CREATE_CARD_SUCCESS = 'CREATE_CARD_SUCCESS';
export const CREATE_CARD_FAILURE = 'CREATE_CARD_FAILURE';

export const fetchCardsRequest = () => ({type: FETCH_CARDS_REQUEST});
export const fetchCardsSuccess = cards => ({type: FETCH_CARDS_SUCCESS, payload: cards});
export const fetchCardsFailure = () => ({type: FETCH_CARDS_FAILURE});

export const createCardRequest = () => ({type: CREATE_CARD_REQUEST});
export const createCardSuccess = () => ({type: CREATE_CARD_SUCCESS});
export const createCardFailure = () => ({type: CREATE_CARD_FAILURE});

export const getCards = () => {
    return async dispatch => {
        try{
            dispatch(fetchCardsRequest())
            const response = await axios.get('http://127.0.0.1:8000/files');
            dispatch(fetchCardsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCardsFailure());
        }

    }
};

export const createCard = cardData => {
    return async dispatch => {
        try {
            dispatch(createCardRequest());
            await axios.post('http://127.0.0.1:8000/files', cardData);
            dispatch(createCardSuccess())
            dispatch(getCards());
        } catch (e) {

        }
    }
}