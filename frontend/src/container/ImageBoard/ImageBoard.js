import React from 'react';
import Cards from "../../components/Cards/Cards";
import CardForm from "../../components/CardForm/CardForm";
import {useDispatch} from "react-redux";
import {createCard} from "../../store/actions";

const ImageBoard = () => {
    const dispatch = useDispatch();

    const createNewCard = cardData => {
        dispatch(createCard(cardData));
    }

    return (
        <div>
            <Cards/>
            <CardForm
                onSubmit={createNewCard}
            />
        </div>
    );
};

export default ImageBoard;