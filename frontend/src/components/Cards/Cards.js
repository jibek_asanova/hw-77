import React, {useEffect} from 'react';
import {getCards} from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@material-ui/core";
import CardItem from "../CardItem/CardItem";

const Cards = () => {
    const dispatch = useDispatch();
    const cards = useSelector(state => state.cards.cards)

    useEffect(() => {
        dispatch(getCards())
    }, [dispatch]);

    return (
        <Grid item container direction="row" spacing={1}>
            {cards.map(product => (
                <CardItem
                    key={product.id}
                    author={product.author}
                    message={product.message}
                    image={product.image}
                />
            ))}
        </Grid>
    );
};

export default Cards;