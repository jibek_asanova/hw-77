import {Card, CardContent, CardHeader, CardMedia, Grid, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {apiUrl} from "../../axiosApi";

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '30.25%'
    },
    text: {
        marginLeft: theme.spacing(2)
    }
}))
const CardItem = ({author, message, image}) => {
    const classes = useStyles();

    const cardImage = apiUrl + '/uploads/' + image;

    return (
        <Grid item xs={12} sm={6} md={4} lg={12}>
            <Card className={classes.card}>
                <CardHeader title={author}/>
                {image ? <CardMedia
                    image={cardImage}
                    title={author}
                    className={classes.media}
                /> : <p className={classes.text}>No image</p>}

                <CardContent>
                    <Typography variant={"subtitle1"}>
                        Message: {message}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default CardItem;